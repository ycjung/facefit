name := "facefit2"

version := "0.1"

scalaVersion := "2.12.4"

resolvers += Resolver.bintrayRepo("unibas-gravis", "maven")
resolvers += "Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.0.0" % "test",
  "ch.unibas.cs.gravis" %% "scalismo-faces" % "0.6.0",
  "ch.unibas.cs.gravis" %% "scalismo" % "0.15.2",
  "ch.unibas.cs.gravis" % "scalismo-native-all" % "4.0.0",
  "ch.unibas.cs.gravis" %% "scalismo-ui" % "0.11.1",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",
  "org.scalanlp" %% "breeze" % "0.13",
  "org.scalanlp" %% "breeze-natives" % "0.13",
  "org.scalanlp" %% "breeze-viz" % "0.13"
)
        