package Scripts

import java.io.{File, PrintWriter}

import com.typesafe.scalalogging.Logger
import scalismo.faces.color.{RGB, RGBA}
import scalismo.faces.image.PixelImage
import scalismo.faces.io.{MoMoIO, PixelImageIO, RenderParameterIO}
import scalismo.faces.parameters.RenderParameter
import scalismo.faces.sampling.face.MoMoRenderer

/**
  * Analyze synthesized rendering parameters and write reports
  *
  * Run this after running BuildGPMM
  **/

object Analyzer extends App {
  private val logger = Logger(Analyzer.getClass)

  def distance(background: PixelImage[RGB], foreground: PixelImage[RGBA]): Double = {
    require(background.domain == foreground.domain)

    val validPixelCount = foreground.toArray.count(p => p.a!=0)
    val pixelDistSum = (background.toArray zip foreground.toArray).foldLeft(0.0){
      case (sum:Double, (b:RGB, f:RGBA)) =>
        if (f.a == 0)
          sum
        else
          sum+(f.toRGB - b).toVector.norm2
    }

    pixelDistSum / validPixelCount
  }

  case class Sample(im: PixelImage[RGB], rp: RenderParameter)

  case class Statistics(
                         sampleSize: Int, mean: Double,
                         sdev: Double
                       ) {
    def mergedWith(st: Statistics): Statistics = {
      val mean = (this.mean*this.sampleSize + st.mean*st.sampleSize) / (this.sampleSize + st.sampleSize)
      val sdev = math.sqrt(math.pow(this.sdev*this.sampleSize,2) + math.pow(st.sdev*st.sampleSize,2)) / (this.sampleSize + st.sampleSize)
      val count = this.sampleSize + st.sampleSize
      Statistics(count, mean, sdev)
    }
  }
  case class Analysis(
                       asianCount: Int,
                       nonAsianCount: Int,
                       asian: Statistics,
                       nonAsian: Statistics,
                     )

  val VERSION = "dense"
  def prefix(asianCount:Int, caucasianCount:Int, runCount:Int) =
    s"${VERSION}_s${asianCount}_c${caucasianCount}_rc$runCount"

  def samples(asianCount: Int, caucasianCount: Int, runCount: Int): (Seq[Sample], Seq[Sample]) = {

    def indToSample(ind: String): Sample = {
      val rpFile = new File(Fitter.resultRPSPath(prefix(asianCount, caucasianCount, runCount), ind, "fa"))
      val imFile = new File(Fitter.resultImagePath(prefix(asianCount, caucasianCount, runCount), ind, "fa"))
      Sample(PixelImageIO.read[RGB](imFile).get, RenderParameterIO.read(rpFile).get)
    }

    val asianSamples = Fitter.eastAsianSubjectInds.map(indToSample)
    val nonAsianSamples = Fitter.nonEastAsianSubjectInds.map(indToSample)
    (asianSamples, nonAsianSamples)
  }

  def statistics(modelRenderer: MoMoRenderer, samples: Seq[Sample]): Statistics = {
    val values = samples.map {
      case Sample(im, rp) =>
        val foreground = modelRenderer.renderImage(rp)
        distance(im, foreground)
    }
    val mean = values.sum/values.size
    val sdev = math.sqrt(values.map(v => math.pow(v - mean,2)).sum) / values.size
    Statistics(values.size, mean, sdev)
  }

  def analyze(): Unit = {
    val as: Seq[Analysis] = (0 to 10).map(_ * 4) map { i =>
      val asianCount = i
      val nonAsianCount = 40 - i

      val tries = (1 to 2).map { runCount =>
        val momoFile = new File(Fitter.modelPath(prefix(asianCount, nonAsianCount, 1)))
        val momo = MoMoIO.read(momoFile).get
        val modelRenderer = MoMoRenderer(momo)

        val (asian, nonAsian) = samples(asianCount, nonAsianCount, runCount)

        val result = (statistics(modelRenderer, asian), statistics(modelRenderer, nonAsian))
        logger.info(s"$asianCount/$nonAsianCount ${result._1.mean} ${result._2.mean}")
        result
      }
      val asian = tries(0)._1 mergedWith tries(1)._1
      val nonAsian = tries(0)._2 mergedWith tries(1)._2

      Analysis(
        asianCount = asianCount,
        nonAsianCount = nonAsianCount,
        asian = asian,
        nonAsian = nonAsian
      )
    }
    val text: String = as.map(a => s"${a.asianCount} ${a.asian.mean} ${a.asian.sdev} ${a.nonAsian.mean} ${a.nonAsian.sdev}").mkString("\n")
    val pw = new PrintWriter(new File("result.txt"))
    pw.write(text)
    pw.close()
  }

  analyze()
}
