package Scripts

import com.typesafe.scalalogging.Logger

/**
  * Runs test on 11 different settings of GPMM.
  */
object SynthGenerator extends App {
  val RUN = 2
  val logger = Logger(SynthGenerator.getClass)
  val VERSION = "dense"

  (0 to 10).map(_ * 4) foreach { i =>
    val asianCount = i
    val caucasianCount = 40 - i
    (1 to RUN) foreach { runCount =>
      val prefix = s"${VERSION}_s${asianCount}_c${caucasianCount}_rc$runCount"
      logger.info(s"running $prefix")
      BuildGPMM.build(prefix, asianCount, caucasianCount)
      Fitter.run(prefix)
    }
  }
}
