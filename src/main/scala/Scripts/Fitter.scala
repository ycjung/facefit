package Scripts

import java.io.{File => JFile}
import java.net.URI

import scala.reflect.io.Path
import scala.concurrent.{Await, Future, Promise, blocking}
import com.typesafe.scalalogging.Logger

import scala.concurrent.ExecutionContext.Implicits._
import scalismo.faces.color.{RGB, RGBA}
import scalismo.faces.image.PixelImage
import scalismo.faces.io._
import scalismo.faces.landmarks.TLMSLandmark2D
import scalismo.faces.parameters.{MoMoInstance, RenderParameter, SphericalHarmonicsLight}
import scalismo.faces.sampling.face.MoMoRenderer
import scalismo.utils.Random
import scalismo.faces.gui.GUIBlock
import scalismo.faces.sampling.face.evaluators.{ImageRendererEvaluator, IndependentPixelEvaluator, LandmarkPointEvaluator}
import scalismo.faces.sampling.face.evaluators.PriorEvaluators.{GaussianShapePrior, GaussianTexturePrior}
import scalismo.sampling.evaluators._
import scalismo.faces.sampling.face.proposals._
import scalismo.sampling.proposals.MixtureProposal
import scalismo.faces.image.PixelImageOperations
import ParameterProposals.implicits._
import ProductEvaluator.implicits._
import scalismo.geometry.{Point3D, Vector2D, Vector3D}
import MixtureProposal.implicits._
import scalismo.faces.sampling.evaluators.CachedDistributionEvaluator.implicits._
import scalismo.common.PointId
import scalismo.faces.sampling.face.proposals.SphericalHarmonicsLightProposals.{SHLightIntensityProposal, SHLightPerturbationProposal}
import scalismo.sampling.algorithms.MetropolisHastings

import scala.concurrent.duration.Duration
import scala.util.Success

/**
  * Fit to test images
  */
object Fitter {
  scalismo.initialize()
  implicit val rnd: Random = Random(27)
  val logger = Logger(Fitter.getClass)
  val pathprefix = "/media/ycjung/98BE-307A/"

  def inputImagePath(ind: String, pose: String) =
    s"/media/ycjung/98BE-307A/feret/colorferet/colorferet/dvd2/data/images/$ind/${ind}_$pose.png"
  def landmarksPath(ind: String, pose: String) =
    s"/media/ycjung/98BE-307A/feret/colorferet/colorferet/dvd2/data/images/$ind/${ind}_$pose.tlms"
  def intermediateRPSPath(prefix: String, ind: String, pose: String) =
    s"${pathprefix}results/$prefix/${ind}_$pose.rps"
  def resultRPSPath(prefix: String, ind: String, pose: String) =
    s"${pathprefix}results/$prefix/${ind}_result_$pose.rps"
  def interResultImagePath(prefix: String, ind: String, pose: String) =
    s"${pathprefix}results/$prefix/${ind}_inter_$pose.png"
  def resultImagePath(prefix: String, ind: String, pose: String) =
    s"${pathprefix}results/$prefix/${ind}_result_$pose.png"
  def resultOrigImagePath(prefix: String, ind: String, pose: String) =
    s"${pathprefix}results/$prefix/${ind}_$pose.png"
  def resultMeshPath(prefix: String, ind: String, pose: String) =
    s"${pathprefix}results/$prefix/${ind}_result_$pose.ply"
  def modelPath(prefix: String) =
    s"${pathprefix}results/$prefix/model.h5"

  val eastAsianSubjectInds = Seq(
    "00740", "00752", "00763", "00784", "00795","00809", "00810",
//    "00856", "00859",
    "00856",
//    "00880","00883", "00893", "00911", "00913",
    "00921","00932", "00933", "00988", "00989", "00992",
  )
  val nonEastAsianSubjectInds = Seq(
    "00741", "00742", "00743", "00744", "00746", "00747", "00749",
    "00818",
//    "00876", "00877", "00878", "00884", "00882",
    "00920", "00922", "00925", "00938", "00939", "00947"
  )
  val poses = Seq("fa")

  type Image = PixelImage[RGBA]
  type Landmarks = IndexedSeq[TLMSLandmark2D]
  case class Canditate(imageFile: JFile,
                       landmarks: Landmarks,
                       subjectIndex: String,
                       pose: String)

  case class FittingResult(renderParameter: RenderParameter,
                           index: String,
                           pose: String)

  private def sampleOfSubjects(inds: Seq[String], poses: Seq[String]) = for{
    i <- inds
    pose <- poses.filter(p => new JFile(inputImagePath(i, p)).exists())
    imageFile = new JFile(inputImagePath(i, pose))
    tlms = TLMSLandmarksIO.read2D(new JFile(landmarksPath(i, pose))).get
  } yield Canditate(imageFile, tlms, i, pose)

  private val asianSamples: Seq[Canditate] = sampleOfSubjects(eastAsianSubjectInds, poses)
  private val nonAsianSamples: Seq[Canditate] = sampleOfSubjects(nonEastAsianSubjectInds, poses)
  val imageSize:(Int, Int) = (512, 768)


  /* Optimization is done through 2 steps
   *
   * 1. perform landmark fitting. save the parameters as .rps file
   * 2. load .rps file and use as an initial settings in landmark+image fitting.
   *
   * Each code for optimization is mostly from http://gravis.dmi.unibas.ch/PMM/
   */

  /* Landmark optimization */

  private def genTLMS(prefix: String, modelRenderer:MoMoRenderer, momoURI: URI): Unit = {
    val default = RenderParameter.default
    val landmarkInitRenderParameters = default
      .forImageSize(imageSize._1, imageSize._2)
      .withMoMo(MoMoInstance.zero(30, 30, 0, momoURI))
    val tasks = (asianSamples ++ nonAsianSamples) map {
      case (Canditate(imageFile, tlms, i, pose)) =>
        Future {
          // Build Target Distribution
          val landmarksEval = LandmarkPointEvaluator.isotropicGaussian(
            tlms,
            4.0,
            modelRenderer
          )
          val prior = GaussianShapePrior(0.0, 1.0)
          val posterior = ProductEvaluator(landmarksEval * prior)

          // Build Proposal distrubution
          val shapeProposal = GaussianMoMoShapeProposal(0.05).toParameterProposal
          val yaw = GaussianRotationProposal(Vector3D.unitY, math.toRadians(0.25))
          val nick = GaussianRotationProposal(Vector3D.unitX, math.toRadians(0.25))
          val roll = GaussianRotationProposal(Vector3D.unitZ, math.toRadians(0.25))
          val rotationProposal = MixtureProposal(yaw + nick + roll).toParameterProposal

          val translationProposal = GaussianTranslationProposal(Vector2D(1, 1)).toParameterProposal

          val focalProposal = GaussianScalingProposal(math.log(1.05)).toParameterProposal

          val distanceProposal = GaussianDistanceProposal(20, compensateScaling = true)

          val poseProposal = MixtureProposal(
            rotationProposal + translationProposal + focalProposal + distanceProposal
          )
          val proposalDistribution = MixtureProposal(
            0.25 *: poseProposal + 0.75 *: shapeProposal
          )

          // Optimize through Metropolis Hastings Algorithm
          val landmarksFitter = MetropolisHastings(
            proposalDistribution,
            posterior
          )
          val iterator = landmarksFitter.iterator(landmarkInitRenderParameters)
          logger.info(s"iterating for $i - $pose")
          val samples = iterator.take(3000).toIndexedSeq
          val posteriorSamples = samples
            .drop(1500)
            .grouped(10).map(_.head).toIndexedSeq
          val bestRenderParameter = posteriorSamples.map { p =>
            (p, posterior.logValue(p))
          }.maxBy {
            _._2
          }._1
          blocking {
            RenderParameterIO.write(bestRenderParameter,
              new JFile(intermediateRPSPath(prefix, i, pose))
            )
          }
          logger.info(s"Saved rms")

          val resultImage = modelRenderer.renderImage(bestRenderParameter)
          val image = PixelImageIO.read[RGB](imageFile).get
          val mixedImage = PixelImageOperations.alphaBlending(image, resultImage)
          blocking {
            PixelImageIO.write[RGB](mixedImage, new JFile(interResultImagePath(prefix, i, pose)))
          }
          logger.info("Landmark Fitting Done")
        }
    }
    tasks.foreach(Await.ready(_, Duration.Inf))
  }


  /* Landmark + Image Optimization */
  def run(prefix: String): Unit = {
    val momoFile = new JFile(modelPath(prefix))
    val momo = MoMoIO.read(momoFile).get
    val momoURI = momoFile.toURI
    val modelRenderer = MoMoRenderer(momo)
    genTLMS(prefix, modelRenderer, momoURI)

    val resultRenderParameters: Seq[FittingResult] =
      (asianSamples ++ nonAsianSamples).grouped(8).flatMap{g =>
        val tasks = g.map {
          case (Canditate(imageFile, tlms, i, pose)) => Future {
            val init = blocking {
              RenderParameterIO.read(new JFile(intermediateRPSPath(prefix, i, pose))).get
            }


            // Build Target Distribution
            val stddevForeground = 0.05
            val stddevBackground = 3.0
            val image = blocking {
              PixelImageIO.read[RGBA](imageFile).get
            }
            val allPixelEval = IndependentPixelEvaluator
              .isotropicGaussianConstantBackground(image, stddevForeground, stddevBackground)
            val imageEval = ImageRendererEvaluator(modelRenderer, allPixelEval)

            val priorShape = GaussianShapePrior(0.0, 1.0)
            val priorTexture = GaussianTexturePrior(0.0, 1.0)
            val prior = ProductEvaluator(priorShape, priorTexture)
            val posterior = ProductEvaluator(prior * imageEval)

            //Build Proposal Distribution
            val yawProposal = GaussianRotationProposal(Vector3D.unitY, math.toRadians(1.0))
            val nickProposal = GaussianRotationProposal(Vector3D.unitX, math.toRadians(1.0))
            val rollProposal = GaussianRotationProposal(Vector3D.unitZ, math.toRadians(1.0))
            val rotationProposal = MixtureProposal(yawProposal + nickProposal + rollProposal).toParameterProposal

            val translationProposal = GaussianTranslationProposal(Vector2D(5, 5)).toParameterProposal

            val scalingProposal = GaussianScalingProposal(math.log(1.05)).toParameterProposal

            val distanceProposal = GaussianDistanceProposal(20, compensateScaling = true)

            val poseProposal = MixtureProposal(
              rotationProposal + translationProposal + scalingProposal + distanceProposal
            )

            val shapeProposal = GaussianMoMoShapeProposal(0.05).toParameterProposal

            val colorProposal = GaussianMoMoColorProposal(0.05).toParameterProposal

            val lightProposal1 = SHLightPerturbationProposal(0.001, fixIntensity = true).toParameterProposal
            val lightProposal2 = SHLightIntensityProposal(0.1).toParameterProposal
            val lightProposal = MixtureProposal(lightProposal1 + lightProposal2)
            MoMoRenderer

            val proposalDistribution = MixtureProposal(
              poseProposal +
                2 *: shapeProposal +
                2 *: colorProposal +
                2 *: lightProposal
            )

            val cachedPosterior = posterior.cached(5)

            val imageSampler = MetropolisHastings(
              proposalDistribution,
              cachedPosterior
            )

            case class RenderParameterSample(number: Int,
                                 sample: RenderParameter,
                                 posteriorValue: Double)

            val posteriorSampleIterator = imageSampler.iterator(init)
            logger.info(s"Landmark+Pixel iterating for $i - $pose")
            val posteriorSamples: IndexedSeq[RenderParameterSample] = posteriorSampleIterator.zipWithIndex.map {
              case (sample, index) =>
                RenderParameterSample(
                  index, sample, cachedPosterior.logValue(sample)
                )
            }.take(3500).toIndexedSeq
            val best = posteriorSamples.maxBy {
              case RenderParameterSample(_, _, v) => v
            }

            logger.info(s"Landmark + Image Fitting Done for $i - $pose")

            FittingResult(best.sample, i, pose)
          }
        }
        //Batch 8 tasks. Wait for each batch to finish
        tasks.map(f => Await.result(f, Duration.Inf))
      }.toSeq

    logger.info(s"len: ${resultRenderParameters.size}")
    print(resultRenderParameters)
    resultRenderParameters foreach {
      case FittingResult(rp, i, pose) =>
        logger.info(s"Finishing $i, $pose")
        val resultImage = modelRenderer.renderImage(rp)
        val imageFile = new JFile(inputImagePath(i, pose))
        val image = PixelImageIO.read[RGBA](imageFile).get
        val model = modelRenderer.renderMesh(rp)
        val mixedImage = PixelImageOperations.alphaBlending(PixelImageOperations.removeAlpha(image), resultImage)
        PixelImageIO.write[RGB](mixedImage, new JFile(resultImagePath(prefix, i, pose)))
        PixelImageIO.write[RGBA](image, new JFile(resultOrigImagePath(prefix, i, pose)))
        RenderParameterIO.write(rp, new JFile(resultRPSPath(prefix, i, pose)))
        MeshIO.write(model, new JFile(resultMeshPath(prefix, i, pose)))
    }
  }
}
