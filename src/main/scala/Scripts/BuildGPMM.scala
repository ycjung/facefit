package Scripts

import java.io.File

import com.typesafe.scalalogging.Logger
import util.ShapeUtil
import scalismo.common.{PointId, UnstructuredPointsDomain}
import scalismo.faces.io.{MeshIO, MoMoIO}
import scalismo.faces.mesh.{OptionalColorNormalMesh3D, VertexColorMesh3D}
import scalismo.faces.momo.{MoMo, MoMoBasic}
import scalismo.faces.render.Transform3D
import scalismo.geometry.{Landmark, Point, Point3D, _3D}
import scalismo.io.LandmarkIO
import scalismo.mesh.TriangleMesh3D

import scala.util.{Random => SRandom}
import scala.util.{Failure, Success}

/**
  * call 'build' to generate GPMM .h5 file to target folder
  */
object BuildGPMM {
  scalismo.initialize()
  val landmarkInds = IndexedSeq(
    2193, 2176, 2804, 1413, 1420, 1203, 1107, 1130, 1374, 4108, 3761, 4012, 3997, 4135, 4239, 4502, 4908,
    2255, 2301, 2298, 2295, 2242, 4994, 5000, 5051, 5064, 5008,
    2229, 1491, 543, 620, 734, 722, 472, 3481, 3508,
    2580, 2574, 2501, 2606, 2408, 2486,
    5240, 5201, 5136, 5167, 5114, 5175,
    913, 187, 124, 2903, 2916, 3027, 2868, 2862, 2956, 906, 1149, 93, 34, 121, 2841, 2909, 2839, 3088, 3047, 261
  )
  val sourcePrefix =
    "/media/ycjung/98BE-307A/FaceWarehouse_Data_0/Tester_%d/TrainingPose/pose_cropped_%d.ply"
  val logger = Logger(BuildGPMM.getClass)

  def build(prefix: String, asianTesterCount: Int, caucasianTesterCount: Int): Unit = {

    def isAsian(index: Int): Boolean= !((101 to 141) contains index)
    val (asianTesterIndices, caucasianTesterIndices) = (1 to 150).partition(isAsian)

    /* MODEL BUILD SETTINGS */
    val ASIAN_TESTER_COUNT = asianTesterCount
    val CAUCASIAN_TESTER_COUNT = caucasianTesterCount
    require(ASIAN_TESTER_COUNT <= 110)
    require(CAUCASIAN_TESTER_COUNT <= 40)
    require(ASIAN_TESTER_COUNT >= 0 && CAUCASIAN_TESTER_COUNT >= 0)
    require(ASIAN_TESTER_COUNT + CAUCASIAN_TESTER_COUNT >= 1)
    logger.info(s"generating $asianTesterCount / $caucasianTesterCount")

    def loadMesh(indices: IndexedSeq[Int], count: Int): IndexedSeq[OptionalColorNormalMesh3D] = {
      implicit val srandom: SRandom = new SRandom(100)
      for {
        testerNum <- srandom.shuffle(indices).take(count)
        poseNum <- 0 until 1
        file = new File(sourcePrefix.format(testerNum, poseNum))
        a = println(sourcePrefix.format(testerNum, poseNum))
      } yield MeshIO.read(file).get
    }

    // Load models
    val asianMeshes =
      loadMesh(asianTesterIndices, ASIAN_TESTER_COUNT)
    val caucasianMeshes =
      loadMesh(caucasianTesterIndices, CAUCASIAN_TESTER_COUNT)

    val allMeshes =
      (asianMeshes ++ caucasianMeshes) map (p => {
        val points = p.shape.pointSet.points.map(p => (p.toVector * 70).toPoint).toIndexedSeq
        val shape = TriangleMesh3D(UnstructuredPointsDomain(points), p.shape.triangulation)
        VertexColorMesh3D(shape, p.toVertexColorMesh3D.get.color)
      })
    val mean = ShapeUtil.meanShape(allMeshes map (_.shape))
    val momo = MoMo.buildFromRegisteredSamples(mean, allMeshes, allMeshes, 0.1, 0.1)
    MoMoIO.write(momo, new File("inter.h5"))
    val a = MoMoIO.read(new File("inter.h5")).get

    val landmarkMap: Map[String, Landmark[_3D]] =
      Map(landmarkInds.indices map (i =>
        i.toString -> Landmark(i.toString, a.referenceMesh.pointSet.point(PointId(landmarkInds(i))))): _*
      )

    new File(s"/media/ycjung/98BE-307A/results/$prefix").mkdir()
    MoMoIO.write(momo.withLandmarks(landmarkMap), new File(s"/media/ycjung/98BE-307A/results/$prefix/model.h5"))
    logger.info(s"Written results/${prefix}_model${asianTesterCount}_$caucasianTesterCount.h5")
  }
}
