package util

import scalismo.common.UnstructuredPointsDomain
import scalismo.geometry.Point
import scalismo.mesh.TriangleMesh3D

object ShapeUtil {
  def meanShape(meshes: IndexedSeq[TriangleMesh3D]):TriangleMesh3D = {
    val p0 = IndexedSeq.fill(meshes(0).pointSet.points.size)(Point(0, 0, 0))
    val pointsSum = meshes.foldLeft(p0)((prev, m) => {
      val points = m.pointSet.points.toIndexedSeq
      points.indices map { i =>
        (prev(i).toVector + points(i).toVector).toPoint
      }
    })
    val pointsMean = pointsSum map (p => (p.toVector/meshes.size).toPoint)
    new TriangleMesh3D(UnstructuredPointsDomain(pointsMean), meshes(0).triangulation)
  }
}
